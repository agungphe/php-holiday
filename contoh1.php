<?php
// source : https://stackoverflow.com/questions/21142917/get-3-next-working-dates-skip-weekends-and-holidays
// $holidays = array('12-24', '12-25');
$holidays = array('2020-08-24', '2020-08-27');
$weekend = array('Sun');
// $date = new DateTime('2013-12-23');
$date = new DateTime();
$nextDay = clone $date;
$i = 0; // We have 0 future dates to start with
$nextDates = array(); // Empty array to hold the next 3 dates
while ($i < 7) {
    $nextDay->add(new DateInterval('P1D')); // Add 1 day
    // if (in_array($nextDay->format('m-d'), $holidays)) continue; // Don't include year to ensure the check is year independent
    if (in_array($nextDay->format('Y-m-d'), $holidays)) continue;
    // Note that you may need to do more complicated things for special holidays that don't use specific dates like "the last Friday of this month"
    if (in_array($nextDay->format('D'), $weekend)) continue;
    // These next lines will only execute if continue isn't called for this iteration
    $nextDates[] = $nextDay->format('Y-m-d');
    $i++;
}

// var_dump($nextDates);

foreach ($nextDates as $n) {
    echo $n . "</br>";
}
